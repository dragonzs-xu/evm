#ifdef CONFIG_EVM_MODULE_I2C
#include "evm_module.h"

void *evm_i2c_open(evm_t *e, evm_val_t obj){
    return NULL;
}

void evm_i2c_read(evm_t *e, void *dev, void *buf, int size){
    
}

void evm_i2c_write(evm_t *e, void *dev, void *buf, int size){
    
}

void evm_i2c_close(evm_t *e, void *dev){
    
}

void evm_i2c_destroy(evm_t *e, void *dev){
    
}
#endif
